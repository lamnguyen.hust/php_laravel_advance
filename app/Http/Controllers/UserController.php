<?php

namespace App\Http\Controllers;
use App\Http\Requests\RegisterRequest;
use App\User;
use App\Classroom;
use App\Events\CreateUser;
use Illuminate\Http\Request;
use App\Http\Requests\EditUserRequest;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('email')->paginate(20);
        //  select('SELECT * FROM users ORDER BY email', [20]);
        return view('login.listUser', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {   
       
        if ($request->id != NULL) {
            $user = User::find($request->id);
        
            if ($request->has('email')) {
                if ($user->email != $request->email) {
                    $user->email = $request->email;
                }
            } 
            if ($request->has('name')) {
                if ($user->name != $request->name) {
                    $user->name = $request->name;
                }
            } 
            if ($request->has('address')) {
                if ($user->address != $request->address) {
                    $user->address = $request->address;
                }
            } 
            if ($request->has('phone')) {
                if (  $user->phone != $request->phone) {
                    $user->phone = $request->phone;
                }  
            } 
            if ($request->password != NULL) {
                if ($request->password_confirmation != NULL){
                    $request->validate([
                         'password' => 'min:3',
                         'password_confirmation' => 'same:password',
                         ]);
                     $user->password = $request->password;
                } else {
                    $request->session()->flash('update_messenger', 'Password Khong hop le!');
                    return redirect()->route('listuser.index');
                }
                
            }
            $user->updated_at = \Carbon\Carbon::now();
            $user->save();
            
            $request->session()->flash('update_messenger', 'Update is success!');
            return redirect()->route('listuser.index');
        } else {
           
            $user = User::create([
                'classroom_id' => $request->select_classroom,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone,
                'role' => $request->select_role,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
            // $user = new User;
            // $user->email = $request->email;
            // $user->password = $request->password;
            // $user->name = $request->name;
            // $user->address = $request->address;
            // $user->phone = $request->phone;
            // $user->created_at = \Carbon\Carbon::now();
            // $user->updated_at = \Carbon\Carbon::now();
            // $user->save();
            $request->session()->flash('status', 'Tạo tài khoản thành công!');
            
            event(new CreateUser($user));
            
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserRequest $request)
    {   
      
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function showRegister() {
        return view('login.register');
    }

    public function findUser(EditUserRequest $request) {
       
        if (isset($request->phone_search) || isset($request->name_search) || isset($request->mail_search)||isset($request->classroom_id_search)) {
            $users = User::popular($request)->paginate(20);
          
            if ($users->total() != 0) {
                $request->session()->flash('update_messenger', 'Find Data Is Success!');
            } else {
                $request->session()->flash('update_messenger', 'Find Data Is False!');
            }
            
        } 
        return view('login.listUser', ['users' => $users]);
    }
}
