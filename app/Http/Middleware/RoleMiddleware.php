<?php

namespace App\Http\Middleware;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
             if (Auth()->user()->role != 'admin') {
                $request->session()->flash('status', 'This Account Cannot Access Register!');
                return redirect()->route('home');
            } elseif (Auth()->user()->classroom_id <= 5) {
                $request->session()->flash('status', 'This Account Has ID < 5 So Cannot Access Register!');
                return redirect()->route('home');
            } 
            else {

                return $next($request);
            }
    
       
    }
}
