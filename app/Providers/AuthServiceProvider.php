<?php

namespace App\Providers;

use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use phpDocumentor\Reflection\Types\True_;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        // Gate::define('access-to-register',function(){
        //     if (Auth()->user()->role == 'admin') {
        //         return TRUE;
        //     } 
        //     return FALSE; 
        // });
        Gate::define('show-register-button', function() {
            if(Auth::user()->role == 'admin') {
                return TRUE;
            } 
            return FALSE;
        });
    }
}
