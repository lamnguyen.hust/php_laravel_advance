<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $table = 'users';
    protected $dateFormat = 'Y-m-d H:i:s';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','address','phone','role','classroom_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function scopePopular($query,$request)
    {
        return $query->where('phone', 'like', '%' .$request->phone_search .'%')
                    ->where('name', 'like', '%' .$request->name_search.'%')
                    ->where('email', 'like', '%' .$request->mail_search.'%');
                    //->where('classroom_id', '%' .$request->classroom_id_search. '%');
                    
    }
    public function classroom() {
        return $this->belongsTo('App\Classroom');
    }
}
