<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Classroom;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Classroom::class, function (Faker $faker) {
    return [
        'name' => strtoupper($faker->bothify('#?')),
    ];
});
    