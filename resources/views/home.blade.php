@extends('layouts.app')

@section('content')
<script type="text/javascript">
    var user = JSON.parse('@json( Auth::user() )') ;

</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! This is Home Page.
                    <br>
                    <a href="{{ url('listuser') }}" >Link To ListUser</a> 
                    
                    <div id="open-component">
                        
                        <profile-component></profile-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
