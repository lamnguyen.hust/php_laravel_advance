@extends('layouts.default')
@section('header-content')
    This is list user 
@endsection
@section('content')
<div class="table-responsive">
  <h2 id="title-content">Table Data User</h2>
    @if (session('status'))
      <div class="alert alert-info">{{session('status')}}</div>
    @endif
    @if (session('update_messenger'))
      <div class="alert alert-info">{{session('update_messenger')}}</div>
    @endif
    <button id="btnSearchUser" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#searchModal"> Search User </button>
    <table class="table" id="table1">
      <thead>
        <tr>
            <th>User ID</th>
            <th>Classroom </th>
            <th>Email Address</th>
            <th>Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Create at</th>
            <th>Update at</th>
            <th>Action</th>
        </tr>
      </thead>
      <tbody>
        
        @foreach($users as $user)
        <tr>
            <th scope="col">{{ $user-> id}}</th>
            <th scope="col">{{ $user->classroom->name}}</th>
            <th scope="col">{{ $user-> email}}</th>
            <th scope="col">{{ $user-> name}}</th>
            <th scope="col">{{ $user-> address}}</th>
            <th scope="col">{{ $user-> phone}}</th>
            <th scope="col">{{ $user-> created_at}}</th>
            <th scope="col">{{ $user-> updated_at}}</th>
        <th scope="col"> <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editModal" onclick="getValueIdUser({{ $user}})"> Edit User </button></th>
        </tr>
        @endforeach
      </tbody>
    </table>
  
    <!-- Modal -->
      <div class="modal fade" id="editModal" role="dialog">
          <div class="modal-dialog">
        
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                <form class="form-content" method="POST" action="{{route('listuser.store')}}">
                    @csrf
                    <div class="row">
                        <div class="form-group">
                         ID : <input id="id_user" name="id" type="text" class="form-control" placeholder="Your Name *" value=""/>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                               <input id="name_user" name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Your Name *" value=""/>
                               <p class="help is-danger">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <input id="phone_user" name="phone" type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone Number *" value=""/>
                                <p class="help is-danger">{{ $errors->first('phone') }}</p>
                            </div>
                            <div class="form-group">
                                <input id="email_user" name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email *" value=""/>
                                <p class="help is-danger">{{ $errors->first('email') }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="fconfirmationorm-group">
                                <input id="address_user" name="address" type="text" class="form-control @error('address') is-invalid @enderror" placeholder="Address *" value=""/>
                                <p class="help is-danger">{{ $errors->first('address') }}</p>
                            </div>
                            <div class="form-group">
                                <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password *" value=""/>
                                <p class="help is-danger">{{ $errors->first('password') }}</p>
                            </div>
                            <div class="form-group">
                                <input name="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Password Confirm *" value=""/>
                                <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
                            </div>
                        </div>
                        <button id="btn-submit-edit" type="submit" class="btnSubmit" name="submit_edit">Confirm Edit</button>
                    </div>    
                </form>     
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
          </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="searchModal" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Search User</h4>
              </div>
              <div class="modal-body">
                <form action="{{route('findUser')}}" method="POST">
                  @csrf
                    <input name="name_search" class="form-control mr-sm-2" type="search" placeholder="Search by Name" aria-label="Search">
                    <input name="mail_search" class="form-control mr-sm-2" type="search" placeholder="Search by Mail" aria-label="Search">
                    <input name="phone_search" class="form-control mr-sm-2" type="search" placeholder="Search by Phone Number" aria-label="Search">
                    <input name="classroom_id_search" class="form-control mr-sm-2" type="search" placeholder="Search by Classroom ID" aria-label="Search">
                    
                    <button id="btn-submit-search" class="btn btn-outline-success my-2 my-sm-0" type="submit"> Search </button>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
      </div>
    <div id="footer">
       <a href="{{ url('register') }}" >Link To Register</a> 
       <span>{{ $users->links() }}</span> 
    </div> 
</div>

<script>
  //   $('#my_modal').on('show.bs.modal', function(e) {
  //     var bookId = $(e.relatedTarget).data('id-User');
  //     $(e.currentTarget).find('input[name="name"]').val(bookId);

  // });
  
  function getValueIdUser(user) {
    var nameUser = document.getElementById('name_user');
    var mailAddressUser = document.getElementById('email_user');
    var addressUser = document.getElementById('address_user');
    var phoneUser = document.getElementById('phone_user');
    var idUser = document.getElementById('id_user');

    nameUser.value = user.name;
    mailAddressUser.value = user.email;
    addressUser.value = user.address;
    phoneUser.value = user.phone;
    idUser.value = user.id;
  }
  </script>
@endsection
    