@extends('layouts.default')

@section('header-content')
    this is register for user
@endsection

@section('content')
<div class="form">
    <div class="note">
        <h2>Form Register</h2>
    </div>
    <form class="form-content" method="POST" action="/register/sent">
      @csrf 
      <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                 <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Your Name *" value=""/>
                 <p class="help is-danger">{{ $errors->first('name') }}</p>
              </div>
              <div class="form-group">
                  <input name="phone" type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone Number *" value=""/>
                  <p class="help is-danger">{{ $errors->first('phone') }}</p>
              </div>
              <div class="form-group">
                  <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email *" value=""/>
                  <p class="help is-danger">{{ $errors->first('email') }}</p>
              </div>
              <div class="form-group">
                <label for="select_role">Select User:</label>
                <select name="select_role" class="form-control" id="select_role">
                  <option>admin</option>
                  <option>user</option>
                </select>

                <label for="select_role">Select Classroom:</label>
                <select name="select_classroom" class="form-control" id="select_classroom">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                  </select>
              </div>
          </div>
          <div class="col-md-6">
               <div class="fconfirmationorm-group">
                  <input name="address" type="text" class="form-control @error('address') is-invalid @enderror" placeholder="Address *" value=""/>
                  <p class="help is-danger">{{ $errors->first('address') }}</p>
              </div>
              <div class="form-group">
                  <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password *" value=""/>
                  <p class="help is-danger">{{ $errors->first('password') }}</p>
              </div>
              <div class="form-group">
                  <input name="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Password Confirm *" value=""/>
                  <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
              </div>
              <button  type="submit" class="btnSubmit" name="submit_register">Submit</button>
          </div>
      </div>    
  </form>     
</div>

@endsection
           