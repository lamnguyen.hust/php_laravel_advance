<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
 


Route::get('/register', 'UserController@showRegister')->middleware('auth','role')->name('showRegisterPHP');
Route::post('/register/sent', 'UserController@store');
Route::resource('listuser', 'UserController')->middleware('auth');
Route::post('listuser/find', 'UserController@findUser')->middleware('auth')->name('findUser');
Route::get('/mail', 'SendEmailController@sendEmail')->name('sendEmail');

